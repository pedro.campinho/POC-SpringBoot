<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ListaFuncionarios</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>

	<div class="container">

		<div id="listaDeFuncionarios">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Nome</th>
						<th>Email</th>
						<th>CPF</th>
						<th>Telefone</th>
					</tr>
				</thead>
				<tr th:each="funcionario : ${funcionarios}">
					<td><span th:text="${funcionario.nome}"></span></td>
					<td><span th:text="${funcionario.email}"></span></td>
					<td><span th:text="${funcionario.cpf}"></span></td>
					<td><span th:text="${funcionario.telefone}"></span></td>
				</tr>

			</table>
		</div>
		
		<hr/>

		<form action="salvar" method="post">
			<div class="form-group">
				<label for="nome">Nome completo</label> <input type="text"
					class="form-control" id="nome" name="nome" placeholder="Nome" />
					<form:errors path="funcionario.nome" />
			</div>
			<div class="form-group">
				<label for="email">E-mail</label> <input type="email"
					class="form-control" id="email" name="email" placeholder="Email" />
			</div>
			<div class="form-group">
				<label for="cpf">CPF</label> <input type="cpf"
					class="form-control" id="cpf" name="cpf" placeholder="XXX.XXX.XXX-XX" />
			</div>
			<div class="form-group">
				<label for="telefone">Telefone</label> <input
					type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" />
			</div>
			<button type="submit" class="btn btn-success">Salvar</button>
		</form>

	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

</body>
</html>