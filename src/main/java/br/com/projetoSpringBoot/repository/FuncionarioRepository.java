package br.com.projetoSpringBoot.repository;


import org.springframework.data.repository.CrudRepository;

import br.com.projetoSpringBoot.model.Funcionario;

public interface FuncionarioRepository extends CrudRepository<Funcionario, Long> {
	
	
//	List<Funcionario> recuperarPorNome(String nome);

}
