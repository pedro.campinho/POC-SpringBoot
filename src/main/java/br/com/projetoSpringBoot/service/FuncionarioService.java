package br.com.projetoSpringBoot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.projetoSpringBoot.model.Funcionario;
import br.com.projetoSpringBoot.repository.FuncionarioRepository;

@Service
public class FuncionarioService {
	
	@Autowired
	private FuncionarioRepository repository;
	
	public Iterable<Funcionario> listar(){
		
		Iterable<Funcionario> convidados = repository.findAll();
		
		return convidados;
	}
	
	
	public void salvar(Funcionario convidado){
		repository.save(convidado);
	}

}
