package br.com.projetoSpringBoot.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;


@Entity
@Table(name="FUNCIONARIO", schema="springboot")
public class Funcionario implements Serializable{
	
	private static final long serialVersionUID = 7312019330787711890L;

	@Id
	@SequenceGenerator(name = "eds_func_seq", sequenceName="eds_func_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="eds_func_seq")
	@Column(name="ID")
	private Long id;
	
	@NotBlank(message = "{name.not.blank}")
	@NotNull(message = "{name.not.blank}")
	@Column(name="NOME")
	private String nome;
	
	@NotBlank(message = "{email.not.blank}")
	@NotNull(message = "{email.not.blank}")
	@Column(name="EMAIL")
	private String email;
	
	@NotBlank(message = "{telefone.not.blank}")
	@NotNull(message = "{telefone.not.blank}")
	@Column(name="TELEFONE")
	private String telefone;
	
	@NotBlank(message = "{cpf.not.blank}")
	@NotNull(message = "{cpf.not.blank}")
	@Column(name="CPF")
	private String cpf;
	
	
	public Funcionario(){
		
	}

	public Funcionario(String nome, String email, String cpf, String telefone) {
		super();
		this.nome = nome;
		this.email = email;
		this.cpf = cpf;
		this.telefone = telefone;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	

}
