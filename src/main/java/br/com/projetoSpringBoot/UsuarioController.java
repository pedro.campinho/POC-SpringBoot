package br.com.projetoSpringBoot;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.projetoSpringBoot.model.Funcionario;
import br.com.projetoSpringBoot.service.FuncionarioService;

@Controller
public class UsuarioController {
 
	
	@Autowired
	private FuncionarioService service;
 
 
 
	/**
	 * CHAMA A FUNCIONALIDADE PARA CADASTRAR UM NOVO USUÁRIO NO SISTEMA
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/novoCadastro", method= RequestMethod.GET)	
	public ModelAndView novoCadastro(Model model) {
 
		/*LISTA DE GRUPOS QUE VAMOS MOSTRAR NA PÁGINA*/
//		model.addAttribute("grupos", grupoService.consultarGrupos());
 
		/*OBJETO QUE VAMOS ATRIBUIR OS VALORES DOS CAMPOS*/
		model.addAttribute("usuarioModel", new Funcionario());
 
	    return new ModelAndView("novoCadastro");
	}
 
 
 
	/**
	 * SALVA UM NOVO USUÁRIO NO SISTEMA
	 * @param usuarioModel
	 * @param result
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value="/salvarUsuario", method= RequestMethod.POST)
	public ModelAndView salvarUsuario(@ModelAttribute("usuarioModel") 
							@Valid Funcionario usuarioModel, 
								final BindingResult result,
									Model model,
										RedirectAttributes redirectAttributes){
		
		if(result.hasErrors()){
			model.addAttribute("usuarioModel", usuarioModel);
			return new ModelAndView("novoCadastro");	
		}
		else{
			service.salvar(usuarioModel);
		}
 
		ModelAndView modelAndView = new ModelAndView("redirect:/listaconvidados");
 
		/*PASSANDO O ATRIBUTO PARA O ModelAndView QUE VAI REALIZAR 
		 * O REDIRECIONAMENTO COM A MENSAGEM DE SUCESSO*/
		redirectAttributes.addFlashAttribute("msg_resultado", "Novo usuário salvo com sucesso!");
 
		/*REDIRECIONANDO PARA UM NOVO CADASTRO*/
		return modelAndView;
	}
}