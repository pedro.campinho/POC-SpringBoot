package br.com.projetoSpringBoot;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.projetoSpringBoot.model.Funcionario;
import br.com.projetoSpringBoot.service.FuncionarioService;

@Controller
public class FuncionarioController {
	
	@Autowired
	private FuncionarioService service;
	
	@RequestMapping("/")
	public String index(){
		return "index";
	}
	
	@RequestMapping("listaconvidados")
	public String listaFuncionarios(Model model){
		
		Iterable<Funcionario> funcionarios = service.listar();
		
		model.addAttribute("funcionarios", funcionarios);
		
		return "listaconvidados";
	}
	
	@RequestMapping(value = "salvar", method = RequestMethod.POST )
	public String salvar(@ModelAttribute("nome") @Valid @RequestParam("nome") String nome, @RequestParam("email")String email, @RequestParam("telefone") String telefone, @RequestParam("cpf") String cpf, Model model){
		
		try{
//			if(result.hasErrors()){
//				return "listaconvidados";
//			}
			Funcionario novoConvidado = new Funcionario(nome, email, cpf, telefone);
			service.salvar(novoConvidado);
			
			//new EmailService().enviar(nome, email);
			Iterable<Funcionario> funcionarios = service.listar();
			
			model.addAttribute("funcionarios", funcionarios);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "listaconvidados";
	}
	

}
